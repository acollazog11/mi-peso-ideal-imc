![Mi Peso Ideal IMC](https://gitlab.com/acollazog11/mi-peso-ideal-imc/-/raw/master/captures/function_graphic.png)


# Mi Peso Ideal IMC 💪

Esta es una aplicación para calcular el [Índice de Masa Corporal (IMC)](https://es.wikipedia.org/wiki/%C3%8Dndice_de_masa_corporal) a partir del peso y altura de la persona. También permite obtener el peso ideal de la persona a partir de su altura.

## Capturas de pantalla


| Splash | Inicio | Historial | Recordatorio | Detalles |
| --- | --- | --- | --- | --- |
| ![splash](https://gitlab.com/acollazog11/mi-peso-ideal-imc/-/raw/master/captures/Screenshot_1.png) | ![home](https://gitlab.com/acollazog11/mi-peso-ideal-imc/-/raw/master/captures/Screenshot_2.png) | ![details](https://gitlab.com/acollazog11/mi-peso-ideal-imc/-/raw/master/captures/Screenshot_3.png) | ![details](https://gitlab.com/acollazog11/mi-peso-ideal-imc/-/raw/master/captures/Screenshot_4.png) | ![details](https://gitlab.com/acollazog11/mi-peso-ideal-imc/-/raw/master/captures/Screenshot_5.png) |

