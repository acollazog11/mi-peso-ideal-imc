import 'package:bmicalculator/src/models/user_data.dart';
import 'package:bmicalculator/src/util/date_converter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  static Future<double> getDouble(String _key, {defaultValue = 170}) async {
    SharedPreferences storage = await SharedPreferences.getInstance();

    String key = 'key_$_key';

    double value =
        storage.containsKey(key) ? storage.getDouble(key) : defaultValue;

    return value;
  }

  static Future<bool> saveDouble(String _key, value) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    String key = 'key_$_key';
    return storage.setDouble(key, value);
  }

  static Future<List<UserData>> getRecords() async {
    SharedPreferences storage = await SharedPreferences.getInstance();

    List<UserData> list = List();
    Set<String> set = storage.getKeys();
    set.forEach((key) {
      if (!key.startsWith('key_')) {
        String value = storage.getString(key) ?? null ?? null;
        if (value != null) {
          list.add(UserData.fromJson(value));
        }
      }
    });
    list.sort((a, b) {
      return b.time.toString().compareTo(a.time.toString());
    });
    return list;
  }

  static Future<bool> saveImcItem(UserData userData) async {
    SharedPreferences storage = await SharedPreferences.getInstance();

    String value = userData.toJson();
    String key = userData.getTimeDayString();

    return storage.setString(key, value);
  }

  static Future<String> getString(String _key, {defaultValue}) async {
    SharedPreferences storage = await SharedPreferences.getInstance();

    String key = 'key_$_key';

    String value =
        storage.containsKey(key) ? storage.getString(key) : defaultValue;

    return value;
  }

  static Future<bool> saveString(String _key, value) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    String key = 'key_$_key';
    return storage.setString(key, value);
  }

  static Future<bool> deleteKey(DateTime dateTime) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    String key = DateConverter.dateTimeToString(dateTime);

    return storage.remove(key);
  }
}
