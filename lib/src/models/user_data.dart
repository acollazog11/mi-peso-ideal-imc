import 'dart:convert';

import 'package:bmicalculator/src/util/date_converter.dart';

class UserData {
  DateTime time;
  final double height;
  final double weight;

  UserData(this.height, this.weight, this.time);

  String toJson() {
    return '{ "height": $height, "weight": $weight, "time": "${time.toString()}" }';
  }

  String getTimeDayString() {
    return time == null ? "-" : DateConverter.dateTimeToString(time);
  }

  static UserData fromJson(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);
    DateTime time;
    String timeString = map.containsKey("time") ? map["time"].toString() : null;
    if (timeString != null) {
      time = DateTime.parse(timeString);
    }
    UserData userData = UserData(map["height"], map["weight"], time);
    return userData;
  }

  @override
  String toString() {
    return '{"height": $height, "weight": $weight}';
  }
}
