import 'dart:convert';

class ReminderData {
  final int day;
  final int hour;
  final int minute;

  ReminderData(this.day, this.hour, this.minute);

  String toJson() {
    return '{ "day": $day, "hour": $hour, "minute": $minute }';
  }

  static ReminderData fromJson(String jsonString) {
    Map<String, dynamic> map = jsonDecode(jsonString);

    ReminderData userData =
        ReminderData(map["day"], map["hour"], map["minute"]);
    return userData;
  }

  @override
  String toString() {
    return '{ "day": $day, "hour": $hour, "minute": "$minute" }';
  }
}
