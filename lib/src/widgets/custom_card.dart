import 'package:bmicalculator/src/values/app_values.dart';
import 'package:flutter/material.dart';

class CustomCard extends StatelessWidget {
  final Widget child;
  final double padding;
  final EdgeInsets margin;
  final double shadowOffset;
  const CustomCard(
      {Key key,
      this.child,
      this.padding = 10,
      this.margin = const EdgeInsets.only(left: 16, right: 16),
      this.shadowOffset = 15})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: whiteColor,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, shadowOffset),
            blurRadius: 30,
            spreadRadius: -25,
            color: blackColor,
          ),
        ],
      ),
      padding: EdgeInsets.all(padding),
      margin: margin,
      child: child,
    );
  }
}
