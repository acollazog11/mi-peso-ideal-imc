const String str_appname = "Mi Peso Ideal IMC";
const String str_app_version = "1.0.7";

const String str_key_reminder_data = "reminder_data";
const String str_key_height = "height";
const String str_key_weight = "weight";

const String str_channel_id = "MI_PESO_IDEAL_IMC_CHANNEL";
const String str_channel_name = "Mi Peso Ideal IMC channel";
const String str_channel_description = "Channel for Mi Peso Ideal IMC";
