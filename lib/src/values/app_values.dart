import 'package:flutter/material.dart';

// colors

const primaryColorHex = 0xFF4E54C8;

const primaryColor = Color(0xFF4E54C8);
const primaryDarkColor = Color(0xFF4E54C8);
const accentColor = Color(0xFF44B3AB);
const dangerColor = Color(0xFFf44336);
// const backgroundColor = Color(0xFFFAFAFA);
const backgroundColor = Color(0xFFF5F5F5);

const heightColor = Colors.teal;
const weightColor = Colors.red;

const whiteColor = Colors.white;
const blackColor = Color(0xFF263238);

// material color
MaterialColor primaryColorMaterial = MaterialColor(
  primaryColorHex,
  <int, Color>{
    50: primaryColor.withOpacity(.1),
    100: primaryColor.withOpacity(.2),
    200: primaryColor.withOpacity(.3),
    300: primaryColor.withOpacity(.4),
    400: primaryColor.withOpacity(.5),
    500: primaryColor.withOpacity(.6),
    600: primaryColor.withOpacity(.7),
    700: primaryColor.withOpacity(.8),
    800: primaryColor.withOpacity(.9),
    900: primaryColor.withOpacity(1),
  },
);

// dimensions
