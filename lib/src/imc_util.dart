import 'package:bmicalculator/src/values/strings.dart';
import 'package:bmicalculator/generated/l10n.dart';

double calculateIMC(double height, double weight) {
  double heightM = height / 100.0;
  return weight / (heightM * heightM);
}

String calculateValueIMC(context, double height, double weight) {
  double heightM = height / 100.0;
  double imcValue = weight / (heightM * heightM);

  String imcText = imcValue.toStringAsFixed(2);

  return Str.of(context).str_imc_imc(imcText);
}

String calculateIconIMC(double height, double weight) {
  double heightM = height / 100.0;
  double imcValue = weight / (heightM * heightM);

  String imcText = "";
  if (imcValue < 18.5)
    imcText = "ic_underweight.svg";
  else if (imcValue >= 18.5 && imcValue < 25)
    imcText = "ic_normal.svg";
  else if (imcValue >= 25 && imcValue < 30)
    imcText = "ic_overweight.svg";
  else if (imcValue >= 30) imcText = "ic_obese.svg";

  return imcText;
}

String calculateMainIMC(context, double height, double weight) {
  double heightM = height / 100.0;
  double imcValue = weight / (heightM * heightM);

  String imcText = "";
  if (imcValue < 18.5)
    imcText = Str.of(context).str_imc_underweight;
  else if (imcValue >= 18.5 && imcValue < 25)
    imcText = Str.of(context).str_imc_normal;
  else if (imcValue >= 25 && imcValue < 30)
    imcText = Str.of(context).str_imc_overweight;
  else if (imcValue >= 30) imcText = Str.of(context).str_imc_obese;

  return imcText;
}

String calculateSecondIMC(context, double height, double weight) {
  String imcText = "-";

  double heightM = height / 100.0;
  double imcValue = weight / (heightM * heightM);

  if (imcValue < 16)
    imcText = Str.of(context).str_imc_underweight_i;
  else if (imcValue >= 16 && imcValue < 17)
    imcText = Str.of(context).str_imc_underweight_ii;
  else if (imcValue >= 17 && imcValue < 18.5)
    imcText = Str.of(context).str_imc_underweight_iii;
  else if (imcValue >= 18.5 && imcValue < 25)
    imcText = Str.of(context).str_imc_normal_i;
  else if (imcValue >= 25 && imcValue < 30)
    imcText = Str.of(context).str_imc_overweight_i;
  else if (imcValue >= 30 && imcValue < 35)
    imcText = Str.of(context).str_imc_obese_i;
  else if (imcValue >= 35 && imcValue < 40)
    imcText = Str.of(context).str_imc_obese_ii;
  else if (imcValue >= 40) imcText = Str.of(context).str_imc_obese_iii;

  return imcText;
}

String calculateMinIdealWeight(double height) {
  double heightM = height / 100.0;

  int min = (19.0 * (heightM * heightM)).round();

  return '$min';
}

String calculateMaxIdealWeight(double height) {
  double heightM = height / 100.0;

  int max = (24.5 * (heightM * heightM)).round();

  return '$max';
}
