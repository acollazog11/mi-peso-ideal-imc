import 'package:bmicalculator/generated/l10n.dart';
import 'package:bmicalculator/src/screens/home/widgets/horizontal_slider.dart';
import 'package:bmicalculator/src/screens/home/widgets/imc_home.dart';
import 'package:bmicalculator/src/screens/info/info_screen.dart';
import 'package:bmicalculator/src/util/storage.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:bmicalculator/src/values/strings.dart';
import 'package:bmicalculator/src/widgets/custom_card.dart';
import 'package:bmicalculator/src/widgets/rounded_buttom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with AutomaticKeepAliveClientMixin<HomeScreen> {
  @override
  bool get wantKeepAlive => true;
  double height = 170;
  double weight = 60;

  @override
  void initState() {
    super.initState();
    Storage.getDouble('height', defaultValue: 170.0)
        .then((value) => setState(() {
              height = value;
            }));
    Storage.getDouble('weight', defaultValue: 60.0)
        .then((value) => setState(() {
              weight = value;
            }));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: 16),
                    buildHeightSlider(size),
                    SizedBox(height: 16),
                    buildWeightSlider(size),
                    SizedBox(height: 16),
                  ],
                ),
              ),
            ),
            Container(
              color: whiteColor,
              padding: EdgeInsets.all(10),
              child: Row(
                children: [
                  Expanded(
                    child: ImcHome(height: height, weight: weight),
                  ),
                  RoundedButton(
                    color: accentColor,
                    text: Str.of(context).str_btn_details,
                    press: submit,
                  ),
                ],
              ),
            )
          ],
        ),
      ],
    );
  }

  void submit() {
    if (validate()) {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => InfoScreen(weight: weight, height: height),
          ));
    }
  }

  bool validate() {
    return weight != null && weight > 0 && height != null && height > 0;
  }

  buildHeightSlider(Size size) {
    final int heightInt = height.round();

    return CustomCard(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Container(
                  height: 100,
                  width: size.width * 0.25,
                  child: SvgPicture.asset(
                    "assets/images/height_icon.svg",
                    fit: BoxFit.contain,
                    alignment: AlignmentDirectional.centerStart,
                  ),
                ),
                Expanded(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Text(
                          '$heightInt ${Str.of(context).str_txt_height_unit}',
                          style: TextStyle(
                              fontSize: 32,
                              fontWeight: FontWeight.bold,
                              color: heightColor),
                        ),
                        Text(
                          Str.of(context).str_txt_height,
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          HorizontalSlider(
            color: heightColor,
            min: 80,
            max: 250,
            value: height,
            onChange: (double val) {
              if (height != val) {
                setState(() {
                  height = val;
                });
                Storage.saveDouble('height', val);
              }
            },
          ),
        ],
      ),
    );
  }

  buildWeightSlider(Size size) {
    final int weightInt = weight.round();

    return CustomCard(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              textDirection: TextDirection.rtl,
              children: <Widget>[
                Container(
                  height: 100,
                  width: size.width * 0.25,
                  child: SvgPicture.asset(
                    "assets/images/weight_icon.svg",
                    fit: BoxFit.contain,
                    alignment: AlignmentDirectional.centerEnd,
                  ),
                ),
                Expanded(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Text(
                          '$weightInt ${Str.of(context).str_txt_weight_unit}',
                          style: TextStyle(
                              fontSize: 32,
                              fontWeight: FontWeight.bold,
                              color: weightColor),
                        ),
                        Text(
                          Str.of(context).str_txt_weight,
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          HorizontalSlider(
            color: weightColor,
            min: 30,
            max: 200,
            value: weight,
            onChange: (double val) {
              if (weight != val) {
                setState(() {
                  weight = val;
                });
                Storage.saveDouble('weight', val);
              }
            },
          ),
        ],
      ),
    );
  }
}
