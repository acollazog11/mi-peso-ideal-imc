import 'package:bmicalculator/src/imc_util.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ImcHome extends StatelessWidget {
  final double height;
  final double weight;

  const ImcHome({Key key, this.height, this.weight}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String icon = 'assets/images/' + calculateIconIMC(height, weight);
    return Row(
      children: [
        Hero(
          tag: "imageHero",
          child: SvgPicture.asset(
            icon,
            height: 50,
            fit: BoxFit.fitHeight,
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  calculateMainIMC(context, height, weight),
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: blackColor,
                  ),
                ),
                Text(
                  calculateSecondIMC(context, height, weight),
                  style: TextStyle(
                    fontSize: 16,
                    color: blackColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
