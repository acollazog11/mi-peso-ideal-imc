import 'package:bmicalculator/src/values/app_values.dart';
import 'package:flutter/material.dart';

class HorizontalSlider extends StatelessWidget {
  final Function onChange;
  final double value;
  final double min;
  final double max;
  final MaterialColor color;

  const HorizontalSlider({
    Key key,
    this.onChange,
    this.value,
    this.min = 0,
    this.max = 100,
    this.color = Colors.blue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
        activeTrackColor: color[500],
        inactiveTrackColor: color[100],
        trackShape: RoundedRectSliderTrackShape(),
        trackHeight: 5.0,
        thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
        thumbColor: color[300],
        overlayColor: color.withAlpha(30),
        overlayShape: RoundSliderOverlayShape(overlayRadius: 10.0),
        tickMarkShape: RoundSliderTickMarkShape(),
        activeTickMarkColor: color[500],
        inactiveTickMarkColor: color[100],
        valueIndicatorShape: PaddleSliderValueIndicatorShape(),
        valueIndicatorColor: color[400],
        valueIndicatorTextStyle: TextStyle(
          color: whiteColor,
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),
      ),
      child: Row(
        children: [
          GestureDetector(
            child: Icon(
              Icons.remove,
              color: color,
              size: 28,
            ),
            onTap: () {
              double val = value.round().toDouble();
              if (val > min) {
                onChange(val - 1);
              }
            },
          ),
          Expanded(
            child: Slider(
              value: value,
              min: min,
              max: max,
              divisions: (max - min).truncate() * 2,
              label: value.round().toString(),
              onChanged: (val) {
                onChange(val);
              },
            ),
          ),
          GestureDetector(
            child: Icon(
              Icons.add,
              color: color,
              size: 28,
            ),
            onTap: () {
              double val = value.round().toDouble();
              if (val < max) {
                onChange(val + 1);
              }
            },
          ),
        ],
      ),
    );
  }
}
