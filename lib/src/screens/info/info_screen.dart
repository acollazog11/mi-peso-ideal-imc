import 'dart:typed_data';
import 'dart:ui';

import 'package:bmicalculator/generated/l10n.dart';
import 'package:bmicalculator/src/imc_util.dart';
import 'package:bmicalculator/src/models/user_data.dart';
import 'package:bmicalculator/src/screens/info/widgets/imc_info.dart';
import 'package:bmicalculator/src/util/date_converter.dart';
import 'package:bmicalculator/src/util/storage.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:bmicalculator/src/values/strings.dart';
import 'package:bmicalculator/src/widgets/custom_card.dart';
import 'package:bmicalculator/src/widgets/rounded_buttom.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InfoScreen extends StatefulWidget {
  final double weight;
  final double height;
  final DateTime time;

  const InfoScreen({Key key, this.weight, this.height, this.time})
      : super(key: key);

  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  GlobalKey globalKey = GlobalKey();

  Future<void> _shareImage() async {
    RenderRepaintBoundary boundary =
        globalKey.currentContext.findRenderObject();
    var image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    await Share.file(
        'Mi Peso Ideal IMC', 'Mi Peso Ideal IMC.png', pngBytes, 'image/png',
        text: 'Mi Peso Ideal IMC');
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    String btnText = widget.time == null
        ? Str.of(context).str_btn_save_record
        : Str.of(context).str_btn_save_delete;
    Color btnColor = widget.time == null ? accentColor : dangerColor;

    String title = widget.time == null
        ? Str.of(context).str_txt_today
        : DateConverter.dateTimeToString(widget.time);

    return RepaintBoundary(
      key: globalKey,
      child: Scaffold(
        appBar: AppBar(
          title: Text(title),
          elevation: 0,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.share,
                color: whiteColor,
              ),
              onPressed: () => _shareImage(),
            )
          ],
        ),
        body: Builder(
          builder: (context) => Container(
            color: backgroundColor,
            child: Stack(
              children: <Widget>[
                SvgPicture.asset(
                  'assets/images/bg_info_image.svg',
                  alignment: Alignment.topCenter,
                  width: size.width,
                  fit: BoxFit.fitWidth,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 16),
                      ImcInfo(
                        height: widget.height,
                        weight: widget.weight,
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          padding: EdgeInsets.symmetric(vertical: 16),
                          child: Column(
                            children: <Widget>[
                              CustomCard(
                                padding: 16,
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          Str.of(context).str_txt_my_data,
                                          style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: blackColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 16),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        _buildMeasureWidget(
                                          'weight_icon.svg',
                                          weightColor,
                                          widget.weight.round().toString() +
                                              ' ${Str.of(context).str_txt_weight_unit}',
                                        ),
                                        _buildMeasureWidget(
                                          'height_icon.svg',
                                          heightColor,
                                          widget.height.round().toString() +
                                              ' ${Str.of(context).str_txt_height_unit}',
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 16),
                              CustomCard(
                                padding: 16,
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          Str.of(context).str_txt_ideal_weight,
                                          style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: blackColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 16),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Text(
                                              calculateMinIdealWeight(
                                                  widget.height),
                                              style: TextStyle(
                                                fontSize: 32,
                                                fontWeight: FontWeight.bold,
                                                color: blackColor,
                                              ),
                                            ),
                                            Text(
                                              ' ${Str.of(context).str_txt_weight_unit}',
                                              style: TextStyle(
                                                fontSize: 18,
                                                color: blackColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                        SvgPicture.asset(
                                          'assets/images/up_chart_icon.svg',
                                          // width: size.width * 0.4,
                                          height: 36,
                                          fit: BoxFit.contain,
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Text(
                                              calculateMaxIdealWeight(
                                                  widget.height),
                                              style: TextStyle(
                                                fontSize: 32,
                                                fontWeight: FontWeight.bold,
                                                color: blackColor,
                                              ),
                                            ),
                                            Text(
                                              ' ${Str.of(context).str_txt_weight_unit}',
                                              style: TextStyle(
                                                fontSize: 18,
                                                color: blackColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Expanded(
                              child: RoundedButton(
                                color: btnColor,
                                text: btnText,
                                press: () => _onButtonPress(context),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onButtonPress(context) async {
    if (widget.time == null) {
      await Storage.saveImcItem(
          UserData(widget.height, widget.weight, DateTime.now()));
      Scaffold.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(
          content: Text(Str.of(context).str_txt_saved_successful),
          backgroundColor: primaryColor,
        ));
    } else {
      _showConfirmDialog(context);
    }
  }

  _goBackWithTrue() {
    Navigator.pop(context, true);
  }

  _showConfirmDialog(context) {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: new Text(Str.of(context).str_txt_confirm_title),
              content: Text(Str.of(context).str_txt_confirm_subtitle),
              actions: <Widget>[
                FlatButton(
                  child: Text(Str.of(context).str_btn_yes),
                  onPressed: () async {
                    await Storage.deleteKey(widget.time);
                    Navigator.pop(context);
                    _goBackWithTrue();
                  },
                ),
                FlatButton(
                  child: Text(Str.of(context).str_btn_no),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
  }

  Widget _buildMeasureWidget(String icon, Color color, String text) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            height: 36,
            width: 36,
            child: SvgPicture.asset(
              'assets/images/$icon',
              fit: BoxFit.contain,
              alignment: AlignmentDirectional.centerEnd,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Text(
              text,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: color,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
