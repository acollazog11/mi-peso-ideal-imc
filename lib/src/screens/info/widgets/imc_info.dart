import 'package:bmicalculator/src/imc_util.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ImcInfo extends StatelessWidget {
  final double height;
  final double weight;

  const ImcInfo({Key key, this.height, this.weight}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String icon = 'assets/images/' + calculateIconIMC(height, weight);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 5,
          child: Hero(
            tag: "imageHero",
            child: SvgPicture.asset(
              icon,
              height: 150,
              fit: BoxFit.fitHeight,
            ),
          ),
        ),
        Expanded(
          flex: 7,
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  calculateValueIMC(context, height, weight),
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 26,
                    // fontWeight: FontWeight.bold,
                    color: whiteColor,
                  ),
                ),
                SizedBox(height: 16),
                Text(
                  calculateMainIMC(context, height, weight),
                  style: TextStyle(
                    fontSize: 26,
                    fontWeight: FontWeight.bold,
                    color: whiteColor,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  calculateSecondIMC(context, height, weight),
                  style: TextStyle(
                    fontSize: 16,
                    color: whiteColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
