import 'package:bmicalculator/src/screens/home/home_screen.dart';
import 'package:bmicalculator/src/screens/record/record_screen.dart';
import 'package:bmicalculator/src/screens/reminder/reminder_screen.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:bmicalculator/src/values/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:bmicalculator/generated/l10n.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  @override
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            indicator: UnderlineTabIndicator(
              borderSide: BorderSide(width: 3.0, color: whiteColor),
            ),
            tabs: [
              Tab(
                text: Str.of(context).str_txt_home,
              ),
              Tab(
                text: Str.of(context).str_txt_record,
              ),
              Tab(
                text: Str.of(context).str_txt_reminder,
              ),
            ],
          ),
          title: Text(Str.of(context).str_appname),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.info_outline,
                color: whiteColor,
              ),
              onPressed: () => _showAboutDialog(context),
            )
          ],
        ),
        body: Container(
          color: backgroundColor,
          child: TabBarView(
            children: [
              HomeScreen(),
              RecordScreen(),
              ReminderScreen(),
            ],
          ),
        ),
      ),
    );
  }

  _showAboutDialog(context) {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              // title: new Text("Material Dialog"),
              content: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        SvgPicture.asset(
                          'assets/images/ic_logo.svg',
                          height: 48,
                          width: 48,
                          fit: BoxFit.contain,
                        ),
                        SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                Str.of(context).str_appname,
                                style: TextStyle(
                                  color: blackColor,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(str_app_version),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 26),
                    Text(Str.of(context).str_txt_description),
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text(Str.of(context).str_btn_privacy_policy),
                  onPressed: () async {
                    if (await canLaunch(
                        Str.of(context).str_url_privacy_policy)) {
                      await launch(Str.of(context).str_url_privacy_policy);
                    }
                  },
                ),
                FlatButton(
                  child: Text(Str.of(context).str_btn_close),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
  }
}
