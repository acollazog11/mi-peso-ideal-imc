import 'package:bmicalculator/src/values/app_values.dart';
import 'package:flutter/material.dart';
import 'package:bmicalculator/generated/l10n.dart';

class WeekdayPicker extends StatefulWidget {
  final int selectedDay;
  final Function onChange;

  const WeekdayPicker({Key key, this.selectedDay = 0, this.onChange})
      : super(key: key);

  @override
  _WeekdayPickerState createState() => _WeekdayPickerState();
}

class _WeekdayPickerState extends State<WeekdayPicker> {
  @override
  Widget build(BuildContext context) {
    List<String> strDayList = Str.of(context).str_array_days.split('-');

    Iterable<Widget> items = Iterable<int>.generate(7)
        .map((index) => _buildDaySelect(strDayList[index], index));

    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Str.of(context).str_txt_day_of_week,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: blackColor,
            ),
          ),
          Wrap(
            children: <Widget>[
              ...items,
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildDaySelect(String day, int index) {
    return Padding(
      padding: EdgeInsets.only(top: 16, right: index < 7 ? 10 : 0),
      child: ClipOval(
        child: Material(
          color: index == widget.selectedDay
              ? primaryColor
              : blackColor.withOpacity(.4), // button color
          child: InkWell(
            splashColor: blackColor.withOpacity(.3), // inkwell color
            child: Container(
              width: 36,
              height: 36,
              child: Center(
                child: Text(
                  day,
                  style: TextStyle(
                    color: whiteColor,
                    // fontSize: 14,
                  ),
                ),
              ),
            ),
            onTap: () {
              widget.onChange(index);
            },
          ),
        ),
      ),
    );
  }
}
