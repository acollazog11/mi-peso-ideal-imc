import 'package:bmicalculator/src/values/app_values.dart';
import 'package:bmicalculator/src/widgets/rounded_buttom.dart';
import 'package:flutter/material.dart';
import 'package:bmicalculator/generated/l10n.dart';

class TimePicker extends StatefulWidget {
  final int hour;
  final int minute;
  final Function onChange;

  const TimePicker({Key key, this.hour = 20, this.minute = 0, this.onChange})
      : super(key: key);

  @override
  _TimePickerState createState() => _TimePickerState();
}

class _TimePickerState extends State<TimePicker> {
  @override
  Widget build(BuildContext context) {
    TimeOfDay _timeOfDay = TimeOfDay(hour: widget.hour, minute: widget.minute);
    String timeString = _formatTimeToString(_timeOfDay);

    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Str.of(context).str_txt_time_of_day,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: blackColor,
            ),
          ),
          SizedBox(height: 10),
          Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30),
              child: FlatButton(
                color: primaryColor,
                textColor: whiteColor,
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                splashColor: blackColor.withOpacity(.3),
                onPressed: () {
                  _selectTime(context, _timeOfDay);
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.alarm,
                    ),
                    SizedBox(width: 10),
                    Text(
                      timeString,
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  String _formatTimeToString(TimeOfDay time) {
    bool isAm = time.hour < 12;
    String sufix = isAm
        ? Str.of(context).str_txt_time_of_day_am
        : Str.of(context).str_txt_time_of_day_pm;
    int h = isAm ? time.hour : time.hour - 12;
    int m = time.minute;
    return '${h.toString().padLeft(2, '0')}:${m.toString().padLeft(2, '0')} $sufix';
  }

  Future<void> _selectTime(BuildContext context, TimeOfDay initTime) async {
    TimeOfDay time = await showTimePicker(
      context: context,
      initialTime: initTime,
    );
    if (time != null && time != initTime) {
      widget.onChange(time.hour, time.minute);
    }
  }
}
