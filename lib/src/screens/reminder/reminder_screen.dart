import 'dart:typed_data';

import 'package:bmicalculator/generated/l10n.dart';
import 'package:bmicalculator/src/models/reminder_data.dart';
import 'package:bmicalculator/src/screens/reminder/widgets/time_picker.dart';
import 'package:bmicalculator/src/screens/reminder/widgets/weekday_picker.dart';
import 'package:bmicalculator/src/util/storage.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:bmicalculator/src/values/strings.dart';
import 'package:bmicalculator/src/widgets/rounded_buttom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class ReminderScreen extends StatefulWidget {
  @override
  _ReminderScreenState createState() => _ReminderScreenState();
}

class _ReminderScreenState extends State<ReminderScreen>
    with AutomaticKeepAliveClientMixin<ReminderScreen> {
  @override
  bool get wantKeepAlive => true;

  bool isActive = false;
  int selectedDay = 0;
  int hour = 19;
  int minute = 0;

  @override
  void initState() {
    super.initState();
    _updateData();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            Text(
              Str.of(context).str_txt_reminder_title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: blackColor,
                fontSize: 20,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: 8),
            Text(
              Str.of(context).str_txt_reminder_subtitle,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: blackColor.withOpacity(0.8),
                fontSize: 14,
              ),
            ),
            SizedBox(height: 36),
            WeekdayPicker(
              selectedDay: selectedDay,
              onChange: (int day) {
                setState(() {
                  selectedDay = day;
                });
              },
            ),
            SizedBox(height: 36),
            TimePicker(
              hour: hour,
              minute: minute,
              onChange: (int h, int m) {
                setState(() {
                  hour = h;
                  minute = m;
                });
              },
            ),
            SizedBox(height: 36),
            Row(
              children: [
                Expanded(
                  child: RoundedButton(
                    color: accentColor,
                    text: isActive
                        ? Str.of(context).str_btn_update
                        : Str.of(context).str_btn_activate,
                    press: () {
                      _showWeeklyAtDayAndTime();
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _updateData() {
    Storage.getString(str_key_reminder_data).then((value) {
      if (value != null) {
        ReminderData rd = ReminderData.fromJson(value);
        setState(() {
          selectedDay = rd.day;
          hour = rd.hour;
          minute = rd.minute;
          isActive = true;
        });
      }
    });
  }

  Future<void> _showWeeklyAtDayAndTime() async {
    _saveReminderData();
    _cancelAllNotifications();

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      str_channel_id,
      str_channel_name,
      str_channel_description,
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.showWeeklyAtDayAndTime(
        0,
        Str.of(context).str_txt_notification_title,
        Str.of(context).str_txt_notification_description,
        Day.values[selectedDay + 1],
        Time(hour, minute, 0),
        platformChannelSpecifics);

    Scaffold.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(
        content: Text(Str.of(context).str_txt_saved_reminder),
        backgroundColor: primaryColor,
      ));
    setState(() {
      isActive = true;
    });
  }

  void _saveReminderData() {
    String value = ReminderData(selectedDay, hour, minute).toJson();
    Storage.saveString(str_key_reminder_data, value);
  }

  Future<void> _cancelAllNotifications() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }
}
