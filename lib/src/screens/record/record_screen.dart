import 'package:bmicalculator/generated/l10n.dart';
import 'package:bmicalculator/src/imc_util.dart';
import 'package:bmicalculator/src/models/user_data.dart';
import 'package:bmicalculator/src/screens/info/info_screen.dart';
import 'package:bmicalculator/src/screens/record/widgets/indicator.dart';
import 'package:bmicalculator/src/screens/record/widgets/record_item.dart';
import 'package:bmicalculator/src/util/storage.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:bmicalculator/src/values/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_timeline/flutter_timeline.dart';
import 'package:flutter_timeline/indicator_position.dart';
import 'package:flutter_timeline/timeline_theme.dart';
import 'package:flutter_timeline/timeline_theme_data.dart';

class RecordScreen extends StatefulWidget {
  @override
  _RecordScreenState createState() => _RecordScreenState();
}

class _RecordScreenState extends State<RecordScreen>
    with WidgetsBindingObserver {
  List<UserData> list;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _updateList();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _updateList();
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final Widget child = (list == null || list.length == 0)
        ? _buildEmptyState(size)
        : _buildTimeline(list);
    return Container(
      child: child,
    );
  }

  _updateList() {
    Storage.getRecords().then((value) {
      setState(() {
        list = value;
      });
    });
  }

  Widget _buildEmptyState(Size size) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          width: size.width * 0.4,
          height: size.width * 0.4,
          child: SvgPicture.asset(
            'assets/images/empty_state1.svg',
            width: size.width * 0.4,
            height: size.width * 0.4,
            fit: BoxFit.contain,
          ),
        ),
        SizedBox(height: 16),
        Text(
          Str.of(context).str_txt_empty_title,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: blackColor,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 10),
        Text(
          Str.of(context).str_txt_empty_subtitle,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: blackColor.withOpacity(0.8),
            fontSize: 14,
          ),
        ),
      ],
    );
  }

  _openDetails(UserData item) async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => InfoScreen(
              weight: item.weight, height: item.height, time: item.time),
        ));
    if (result != null && result == true) {
      Scaffold.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(
          content: Text(Str.of(context).str_txt_deleted_successful),
          backgroundColor: primaryColor,
        ));
      _updateList();
    }
  }

  Widget _buildTimeline(List<UserData> list) {
    List<TimelineEventDisplay> events = List();
    for (var i = 0; i < list.length; i++) {
      UserData item = list[i];
      UserData previousItem = i + 1 < list.length ? list[i + 1] : null;
      events.add(TimelineEventDisplay(
        child: RecordItem(
          item: item,
          previousItem: previousItem,
          onPress: (item) => _openDetails(item),
        ),
        indicator: Indicator(item, previousItem),
      ));
    }

    return TimelineTheme(
        data: TimelineThemeData(
          gutterSpacing: 16,
          lineColor: accentColor.withOpacity(0.3),
          itemGap: 16,
          lineGap: 3,
          strokeWidth: 3,
        ),
        child: Timeline(
          padding: EdgeInsets.all(16),
          indicatorSize: 36,
          events: events,
        ));
  }
}
