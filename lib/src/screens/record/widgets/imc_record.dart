import 'package:bmicalculator/src/imc_util.dart';
import 'package:bmicalculator/src/models/user_data.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ImcRecord extends StatelessWidget {
  final UserData userData;

  const ImcRecord({Key key, this.userData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String icon =
        'assets/images/' + calculateIconIMC(userData.height, userData.weight);
    return Row(
      children: [
        SvgPicture.asset(
          icon,
          height: 48,
          fit: BoxFit.fitHeight,
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  calculateMainIMC(context, userData.height, userData.weight),
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: blackColor,
                  ),
                ),
                Text(
                  calculateValueIMC(context, userData.height, userData.weight),
                  style: TextStyle(
                    fontSize: 14,
                    color: blackColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
