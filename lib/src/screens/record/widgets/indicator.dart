import 'package:bmicalculator/src/imc_util.dart';
import 'package:bmicalculator/src/models/user_data.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:flutter/material.dart';

class Indicator extends StatelessWidget {
  final UserData item;
  final UserData previousItem;

  const Indicator(this.item, this.previousItem);

  @override
  Widget build(BuildContext context) {
    double imc = calculateIMC(item.height, item.weight);
    double previousImc = previousItem == null
        ? -50
        : calculateIMC(previousItem.height, previousItem.weight);

    bool up = imc > previousImc;

    return Container(
      decoration: BoxDecoration(
        color: accentColor,
        borderRadius: BorderRadius.circular(36),
      ),
      child: Center(
        child: Icon(
          up ? Icons.trending_up : Icons.trending_down,
          color: whiteColor,
        ),
      ),
    );
  }
}
