import 'package:bmicalculator/generated/l10n.dart';
import 'package:bmicalculator/src/models/user_data.dart';
import 'package:bmicalculator/src/screens/info/info_screen.dart';
import 'package:bmicalculator/src/screens/record/widgets/imc_record.dart';
import 'package:bmicalculator/src/values/app_values.dart';
import 'package:bmicalculator/src/values/strings.dart';
import 'package:bmicalculator/src/widgets/custom_card.dart';
import 'package:flutter/material.dart';

class RecordItem extends StatelessWidget {
  final UserData item;
  final UserData previousItem;
  final Function onPress;

  RecordItem({this.item, this.previousItem, this.onPress});

  @override
  Widget build(BuildContext context) {
    return CustomCard(
      padding: 2,
      margin: EdgeInsets.all(0),
      shadowOffset: 10,
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () => onPress(item),
          child: Padding(
            padding: const EdgeInsets.all(14),
            child: Row(
              children: [
                Expanded(
                  child: ImcRecord(
                    userData: item,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      item.getTimeDayString(),
                      style: TextStyle(
                        fontSize: 14,
                        // fontWeight: FontWeight.bold,
                        color: blackColor,
                      ),
                    ),
                    Text(
                      item.weight.round().toString() +
                          ' ${Str.of(context).str_txt_weight_unit}',
                      style: TextStyle(
                        fontSize: 14,
                        color: blackColor,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
