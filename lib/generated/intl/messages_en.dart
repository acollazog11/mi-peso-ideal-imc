// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(value) => "IMC: ${value}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "str_app_subtitle" : MessageLookupByLibrary.simpleMessage("Body Mass Index"),
    "str_appname" : MessageLookupByLibrary.simpleMessage("Mi Peso Ideal IMC"),
    "str_array_days" : MessageLookupByLibrary.simpleMessage("Mo-Tu-We-Th-Fr-Sa-Su"),
    "str_btn_activate" : MessageLookupByLibrary.simpleMessage("ACTIVATE REMINDER"),
    "str_btn_close" : MessageLookupByLibrary.simpleMessage("Close"),
    "str_btn_details" : MessageLookupByLibrary.simpleMessage("DETAILS"),
    "str_btn_no" : MessageLookupByLibrary.simpleMessage("No"),
    "str_btn_privacy_policy" : MessageLookupByLibrary.simpleMessage("Privacy Policy"),
    "str_btn_save_delete" : MessageLookupByLibrary.simpleMessage("DELETE"),
    "str_btn_save_record" : MessageLookupByLibrary.simpleMessage("SAVE"),
    "str_btn_update" : MessageLookupByLibrary.simpleMessage("UPDATE REMINDER"),
    "str_btn_yes" : MessageLookupByLibrary.simpleMessage("Yes, Sure"),
    "str_imc_imc" : m0,
    "str_imc_normal" : MessageLookupByLibrary.simpleMessage("Normal"),
    "str_imc_normal_i" : MessageLookupByLibrary.simpleMessage("Ideal weight"),
    "str_imc_obese" : MessageLookupByLibrary.simpleMessage("Obesity"),
    "str_imc_obese_i" : MessageLookupByLibrary.simpleMessage("Mild obesity"),
    "str_imc_obese_ii" : MessageLookupByLibrary.simpleMessage("Medium obesity"),
    "str_imc_obese_iii" : MessageLookupByLibrary.simpleMessage("Severe obesity"),
    "str_imc_overweight" : MessageLookupByLibrary.simpleMessage("Overweight"),
    "str_imc_overweight_i" : MessageLookupByLibrary.simpleMessage("Overweight"),
    "str_imc_underweight" : MessageLookupByLibrary.simpleMessage("Low weight"),
    "str_imc_underweight_i" : MessageLookupByLibrary.simpleMessage("Extreme thinness"),
    "str_imc_underweight_ii" : MessageLookupByLibrary.simpleMessage("Moderate thinness"),
    "str_imc_underweight_iii" : MessageLookupByLibrary.simpleMessage("Slight thinness"),
    "str_txt_confirm_subtitle" : MessageLookupByLibrary.simpleMessage("Are you sure you want to delete this record?"),
    "str_txt_confirm_title" : MessageLookupByLibrary.simpleMessage("Confirm"),
    "str_txt_day_of_week" : MessageLookupByLibrary.simpleMessage("Day of the week:"),
    "str_txt_deleted_successful" : MessageLookupByLibrary.simpleMessage("The record was successfully deleted"),
    "str_txt_description" : MessageLookupByLibrary.simpleMessage("Mi Peso Ideal IMC is an application to calculate the body mass index from the weight (kg) and height (cm) of the person.\n\nIn addition, you will be able to obtain the ideal weight range for their height and keep a record historical."),
    "str_txt_empty_subtitle" : MessageLookupByLibrary.simpleMessage("You can save your first record\nfrom the details view"),
    "str_txt_empty_title" : MessageLookupByLibrary.simpleMessage("You don\'t have records yet"),
    "str_txt_height" : MessageLookupByLibrary.simpleMessage("Height"),
    "str_txt_height_unit" : MessageLookupByLibrary.simpleMessage("cm"),
    "str_txt_home" : MessageLookupByLibrary.simpleMessage("Home"),
    "str_txt_ideal_weight" : MessageLookupByLibrary.simpleMessage("My ideal weight"),
    "str_txt_my_data" : MessageLookupByLibrary.simpleMessage("My data"),
    "str_txt_notification_description" : MessageLookupByLibrary.simpleMessage("Remember to update your weight."),
    "str_txt_notification_title" : MessageLookupByLibrary.simpleMessage("Reminder:"),
    "str_txt_record" : MessageLookupByLibrary.simpleMessage("History"),
    "str_txt_reminder" : MessageLookupByLibrary.simpleMessage("Reminder"),
    "str_txt_reminder_subtitle" : MessageLookupByLibrary.simpleMessage("Select the day and time to set a reminder"),
    "str_txt_reminder_title" : MessageLookupByLibrary.simpleMessage("Set reminder"),
    "str_txt_saved_reminder" : MessageLookupByLibrary.simpleMessage("The reminder was defined successfully"),
    "str_txt_saved_successful" : MessageLookupByLibrary.simpleMessage("The data was saved successfully"),
    "str_txt_time_of_day" : MessageLookupByLibrary.simpleMessage("Time of the day:"),
    "str_txt_time_of_day_am" : MessageLookupByLibrary.simpleMessage("am"),
    "str_txt_time_of_day_pm" : MessageLookupByLibrary.simpleMessage("pm"),
    "str_txt_today" : MessageLookupByLibrary.simpleMessage("Today"),
    "str_txt_weight" : MessageLookupByLibrary.simpleMessage("Weight"),
    "str_txt_weight_unit" : MessageLookupByLibrary.simpleMessage("kg"),
    "str_url_privacy_policy" : MessageLookupByLibrary.simpleMessage("https://sites.google.com/view/mi-peso-ideal-imc/privacy-policy")
  };
}
