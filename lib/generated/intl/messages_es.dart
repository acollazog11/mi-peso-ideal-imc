// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  static m0(value) => "IMC: ${value}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "str_app_subtitle" : MessageLookupByLibrary.simpleMessage("Índice de Masa Corporal"),
    "str_appname" : MessageLookupByLibrary.simpleMessage("Mi Peso Ideal IMC"),
    "str_array_days" : MessageLookupByLibrary.simpleMessage("Lu-Ma-Mi-Ju-Vi-Sa-Do"),
    "str_btn_activate" : MessageLookupByLibrary.simpleMessage("ACTIVAR RECORDATORIO"),
    "str_btn_close" : MessageLookupByLibrary.simpleMessage("Cerrar"),
    "str_btn_details" : MessageLookupByLibrary.simpleMessage("DETALLES"),
    "str_btn_no" : MessageLookupByLibrary.simpleMessage("No"),
    "str_btn_privacy_policy" : MessageLookupByLibrary.simpleMessage("Política de Privacidad"),
    "str_btn_save_delete" : MessageLookupByLibrary.simpleMessage("ELIMINAR"),
    "str_btn_save_record" : MessageLookupByLibrary.simpleMessage("GUARDAR"),
    "str_btn_update" : MessageLookupByLibrary.simpleMessage("ACTUALIZAR RECORDATORIO"),
    "str_btn_yes" : MessageLookupByLibrary.simpleMessage("Si, Seguro"),
    "str_imc_imc" : m0,
    "str_imc_normal" : MessageLookupByLibrary.simpleMessage("Normal"),
    "str_imc_normal_i" : MessageLookupByLibrary.simpleMessage("Peso ideal"),
    "str_imc_obese" : MessageLookupByLibrary.simpleMessage("Obesidad"),
    "str_imc_obese_i" : MessageLookupByLibrary.simpleMessage("Obesidad leve"),
    "str_imc_obese_ii" : MessageLookupByLibrary.simpleMessage("Obesidad media"),
    "str_imc_obese_iii" : MessageLookupByLibrary.simpleMessage("Obesidad grave"),
    "str_imc_overweight" : MessageLookupByLibrary.simpleMessage("Sobrepeso"),
    "str_imc_overweight_i" : MessageLookupByLibrary.simpleMessage("Sobrepeso"),
    "str_imc_underweight" : MessageLookupByLibrary.simpleMessage("Bajo peso"),
    "str_imc_underweight_i" : MessageLookupByLibrary.simpleMessage("Delgadez extrema"),
    "str_imc_underweight_ii" : MessageLookupByLibrary.simpleMessage("Delgadez moderada"),
    "str_imc_underweight_iii" : MessageLookupByLibrary.simpleMessage("Delgadez leve"),
    "str_txt_confirm_subtitle" : MessageLookupByLibrary.simpleMessage("Está seguro que desea eliminar este registro?"),
    "str_txt_confirm_title" : MessageLookupByLibrary.simpleMessage("Confirmar"),
    "str_txt_day_of_week" : MessageLookupByLibrary.simpleMessage("Día de la semana:"),
    "str_txt_deleted_successful" : MessageLookupByLibrary.simpleMessage("El registro se eliminó correctamente"),
    "str_txt_description" : MessageLookupByLibrary.simpleMessage("Mi Peso Ideal es una aplicación para calcular el índice de masa corporal a partir del peso (kg) y altura (cm) de la persona.\n\nAdemás, podrás obtener el rango de peso ideal para su estatura y llevar un registro histórico."),
    "str_txt_empty_subtitle" : MessageLookupByLibrary.simpleMessage("Puedes guardar tu primer registro\ndesde la vista de detalles"),
    "str_txt_empty_title" : MessageLookupByLibrary.simpleMessage("Aún no tienes registros"),
    "str_txt_height" : MessageLookupByLibrary.simpleMessage("Altura"),
    "str_txt_height_unit" : MessageLookupByLibrary.simpleMessage("cm"),
    "str_txt_home" : MessageLookupByLibrary.simpleMessage("Inicio"),
    "str_txt_ideal_weight" : MessageLookupByLibrary.simpleMessage("Mi peso ideal"),
    "str_txt_my_data" : MessageLookupByLibrary.simpleMessage("Mis datos"),
    "str_txt_notification_description" : MessageLookupByLibrary.simpleMessage("Recuerda actualizar tu peso"),
    "str_txt_notification_title" : MessageLookupByLibrary.simpleMessage("Recordatorio:"),
    "str_txt_record" : MessageLookupByLibrary.simpleMessage("Historial"),
    "str_txt_reminder" : MessageLookupByLibrary.simpleMessage("Recordatorio"),
    "str_txt_reminder_subtitle" : MessageLookupByLibrary.simpleMessage("Selecciona el día y la hora para configurar un recordatoio"),
    "str_txt_reminder_title" : MessageLookupByLibrary.simpleMessage("Configurar recordatorio"),
    "str_txt_saved_reminder" : MessageLookupByLibrary.simpleMessage("El recordatorio fue definido correctamente"),
    "str_txt_saved_successful" : MessageLookupByLibrary.simpleMessage("Los datos se guardaron correctamente"),
    "str_txt_time_of_day" : MessageLookupByLibrary.simpleMessage("Hora del día:"),
    "str_txt_time_of_day_am" : MessageLookupByLibrary.simpleMessage("am"),
    "str_txt_time_of_day_pm" : MessageLookupByLibrary.simpleMessage("pm"),
    "str_txt_today" : MessageLookupByLibrary.simpleMessage("Hoy"),
    "str_txt_weight" : MessageLookupByLibrary.simpleMessage("Peso"),
    "str_txt_weight_unit" : MessageLookupByLibrary.simpleMessage("kg"),
    "str_url_privacy_policy" : MessageLookupByLibrary.simpleMessage("https://sites.google.com/view/mi-peso-ideal-imc/privacy-policy")
  };
}
