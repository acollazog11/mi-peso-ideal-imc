// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class Str {
  Str();
  
  static Str current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<Str> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      Str.current = Str();
      
      return Str.current;
    });
  } 

  static Str of(BuildContext context) {
    return Localizations.of<Str>(context, Str);
  }

  /// `Mi Peso Ideal IMC`
  String get str_appname {
    return Intl.message(
      'Mi Peso Ideal IMC',
      name: 'str_appname',
      desc: '',
      args: [],
    );
  }

  /// `Body Mass Index`
  String get str_app_subtitle {
    return Intl.message(
      'Body Mass Index',
      name: 'str_app_subtitle',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get str_txt_home {
    return Intl.message(
      'Home',
      name: 'str_txt_home',
      desc: '',
      args: [],
    );
  }

  /// `History`
  String get str_txt_record {
    return Intl.message(
      'History',
      name: 'str_txt_record',
      desc: '',
      args: [],
    );
  }

  /// `Height`
  String get str_txt_height {
    return Intl.message(
      'Height',
      name: 'str_txt_height',
      desc: '',
      args: [],
    );
  }

  /// `Weight`
  String get str_txt_weight {
    return Intl.message(
      'Weight',
      name: 'str_txt_weight',
      desc: '',
      args: [],
    );
  }

  /// `cm`
  String get str_txt_height_unit {
    return Intl.message(
      'cm',
      name: 'str_txt_height_unit',
      desc: '',
      args: [],
    );
  }

  /// `kg`
  String get str_txt_weight_unit {
    return Intl.message(
      'kg',
      name: 'str_txt_weight_unit',
      desc: '',
      args: [],
    );
  }

  /// `Low weight`
  String get str_imc_underweight {
    return Intl.message(
      'Low weight',
      name: 'str_imc_underweight',
      desc: '',
      args: [],
    );
  }

  /// `Extreme thinness`
  String get str_imc_underweight_i {
    return Intl.message(
      'Extreme thinness',
      name: 'str_imc_underweight_i',
      desc: '',
      args: [],
    );
  }

  /// `Moderate thinness`
  String get str_imc_underweight_ii {
    return Intl.message(
      'Moderate thinness',
      name: 'str_imc_underweight_ii',
      desc: '',
      args: [],
    );
  }

  /// `Slight thinness`
  String get str_imc_underweight_iii {
    return Intl.message(
      'Slight thinness',
      name: 'str_imc_underweight_iii',
      desc: '',
      args: [],
    );
  }

  /// `Normal`
  String get str_imc_normal {
    return Intl.message(
      'Normal',
      name: 'str_imc_normal',
      desc: '',
      args: [],
    );
  }

  /// `Ideal weight`
  String get str_imc_normal_i {
    return Intl.message(
      'Ideal weight',
      name: 'str_imc_normal_i',
      desc: '',
      args: [],
    );
  }

  /// `Overweight`
  String get str_imc_overweight {
    return Intl.message(
      'Overweight',
      name: 'str_imc_overweight',
      desc: '',
      args: [],
    );
  }

  /// `Overweight`
  String get str_imc_overweight_i {
    return Intl.message(
      'Overweight',
      name: 'str_imc_overweight_i',
      desc: '',
      args: [],
    );
  }

  /// `Obesity`
  String get str_imc_obese {
    return Intl.message(
      'Obesity',
      name: 'str_imc_obese',
      desc: '',
      args: [],
    );
  }

  /// `Mild obesity`
  String get str_imc_obese_i {
    return Intl.message(
      'Mild obesity',
      name: 'str_imc_obese_i',
      desc: '',
      args: [],
    );
  }

  /// `Medium obesity`
  String get str_imc_obese_ii {
    return Intl.message(
      'Medium obesity',
      name: 'str_imc_obese_ii',
      desc: '',
      args: [],
    );
  }

  /// `Severe obesity`
  String get str_imc_obese_iii {
    return Intl.message(
      'Severe obesity',
      name: 'str_imc_obese_iii',
      desc: '',
      args: [],
    );
  }

  /// `IMC: {value}`
  String str_imc_imc(Object value) {
    return Intl.message(
      'IMC: $value',
      name: 'str_imc_imc',
      desc: '',
      args: [value],
    );
  }

  /// `Confirm`
  String get str_txt_confirm_title {
    return Intl.message(
      'Confirm',
      name: 'str_txt_confirm_title',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete this record?`
  String get str_txt_confirm_subtitle {
    return Intl.message(
      'Are you sure you want to delete this record?',
      name: 'str_txt_confirm_subtitle',
      desc: '',
      args: [],
    );
  }

  /// `My data`
  String get str_txt_my_data {
    return Intl.message(
      'My data',
      name: 'str_txt_my_data',
      desc: '',
      args: [],
    );
  }

  /// `My ideal weight`
  String get str_txt_ideal_weight {
    return Intl.message(
      'My ideal weight',
      name: 'str_txt_ideal_weight',
      desc: '',
      args: [],
    );
  }

  /// `Today`
  String get str_txt_today {
    return Intl.message(
      'Today',
      name: 'str_txt_today',
      desc: '',
      args: [],
    );
  }

  /// `You don't have records yet`
  String get str_txt_empty_title {
    return Intl.message(
      'You don\'t have records yet',
      name: 'str_txt_empty_title',
      desc: '',
      args: [],
    );
  }

  /// `You can save your first record\nfrom the details view`
  String get str_txt_empty_subtitle {
    return Intl.message(
      'You can save your first record\nfrom the details view',
      name: 'str_txt_empty_subtitle',
      desc: '',
      args: [],
    );
  }

  /// `The record was successfully deleted`
  String get str_txt_deleted_successful {
    return Intl.message(
      'The record was successfully deleted',
      name: 'str_txt_deleted_successful',
      desc: '',
      args: [],
    );
  }

  /// `The data was saved successfully`
  String get str_txt_saved_successful {
    return Intl.message(
      'The data was saved successfully',
      name: 'str_txt_saved_successful',
      desc: '',
      args: [],
    );
  }

  /// `The reminder was defined successfully`
  String get str_txt_saved_reminder {
    return Intl.message(
      'The reminder was defined successfully',
      name: 'str_txt_saved_reminder',
      desc: '',
      args: [],
    );
  }

  /// `Reminder`
  String get str_txt_reminder {
    return Intl.message(
      'Reminder',
      name: 'str_txt_reminder',
      desc: '',
      args: [],
    );
  }

  /// `Set reminder`
  String get str_txt_reminder_title {
    return Intl.message(
      'Set reminder',
      name: 'str_txt_reminder_title',
      desc: '',
      args: [],
    );
  }

  /// `Select the day and time to set a reminder`
  String get str_txt_reminder_subtitle {
    return Intl.message(
      'Select the day and time to set a reminder',
      name: 'str_txt_reminder_subtitle',
      desc: '',
      args: [],
    );
  }

  /// `Day of the week:`
  String get str_txt_day_of_week {
    return Intl.message(
      'Day of the week:',
      name: 'str_txt_day_of_week',
      desc: '',
      args: [],
    );
  }

  /// `Time of the day:`
  String get str_txt_time_of_day {
    return Intl.message(
      'Time of the day:',
      name: 'str_txt_time_of_day',
      desc: '',
      args: [],
    );
  }

  /// `am`
  String get str_txt_time_of_day_am {
    return Intl.message(
      'am',
      name: 'str_txt_time_of_day_am',
      desc: '',
      args: [],
    );
  }

  /// `pm`
  String get str_txt_time_of_day_pm {
    return Intl.message(
      'pm',
      name: 'str_txt_time_of_day_pm',
      desc: '',
      args: [],
    );
  }

  /// `Reminder:`
  String get str_txt_notification_title {
    return Intl.message(
      'Reminder:',
      name: 'str_txt_notification_title',
      desc: '',
      args: [],
    );
  }

  /// `Remember to update your weight.`
  String get str_txt_notification_description {
    return Intl.message(
      'Remember to update your weight.',
      name: 'str_txt_notification_description',
      desc: '',
      args: [],
    );
  }

  /// `ACTIVATE REMINDER`
  String get str_btn_activate {
    return Intl.message(
      'ACTIVATE REMINDER',
      name: 'str_btn_activate',
      desc: '',
      args: [],
    );
  }

  /// `UPDATE REMINDER`
  String get str_btn_update {
    return Intl.message(
      'UPDATE REMINDER',
      name: 'str_btn_update',
      desc: '',
      args: [],
    );
  }

  /// `DETAILS`
  String get str_btn_details {
    return Intl.message(
      'DETAILS',
      name: 'str_btn_details',
      desc: '',
      args: [],
    );
  }

  /// `SAVE`
  String get str_btn_save_record {
    return Intl.message(
      'SAVE',
      name: 'str_btn_save_record',
      desc: '',
      args: [],
    );
  }

  /// `DELETE`
  String get str_btn_save_delete {
    return Intl.message(
      'DELETE',
      name: 'str_btn_save_delete',
      desc: '',
      args: [],
    );
  }

  /// `Close`
  String get str_btn_close {
    return Intl.message(
      'Close',
      name: 'str_btn_close',
      desc: '',
      args: [],
    );
  }

  /// `Privacy Policy`
  String get str_btn_privacy_policy {
    return Intl.message(
      'Privacy Policy',
      name: 'str_btn_privacy_policy',
      desc: '',
      args: [],
    );
  }

  /// `Yes, Sure`
  String get str_btn_yes {
    return Intl.message(
      'Yes, Sure',
      name: 'str_btn_yes',
      desc: '',
      args: [],
    );
  }

  /// `No`
  String get str_btn_no {
    return Intl.message(
      'No',
      name: 'str_btn_no',
      desc: '',
      args: [],
    );
  }

  /// `https://sites.google.com/view/mi-peso-ideal-imc/privacy-policy`
  String get str_url_privacy_policy {
    return Intl.message(
      'https://sites.google.com/view/mi-peso-ideal-imc/privacy-policy',
      name: 'str_url_privacy_policy',
      desc: '',
      args: [],
    );
  }

  /// `Mi Peso Ideal IMC is an application to calculate the body mass index from the weight (kg) and height (cm) of the person.\n\nIn addition, you will be able to obtain the ideal weight range for their height and keep a record historical.`
  String get str_txt_description {
    return Intl.message(
      'Mi Peso Ideal IMC is an application to calculate the body mass index from the weight (kg) and height (cm) of the person.\n\nIn addition, you will be able to obtain the ideal weight range for their height and keep a record historical.',
      name: 'str_txt_description',
      desc: '',
      args: [],
    );
  }

  /// `Mo-Tu-We-Th-Fr-Sa-Su`
  String get str_array_days {
    return Intl.message(
      'Mo-Tu-We-Th-Fr-Sa-Su',
      name: 'str_array_days',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<Str> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'es'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<Str> load(Locale locale) => Str.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}